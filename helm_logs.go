package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"encoding/json"	
	"os/exec"
	"path/filepath"
)


type HelmReleaseStatus struct {
	Name string `json:"name"`
	Info struct {
		Status struct {
			Code      int    `json:"code"`
			Resources string `json:"resources"`
			Notes     string `json:"notes"`
		} `json:"status"`
		FirstDeployed struct {
			Seconds int `json:"seconds"`
			Nanos   int `json:"nanos"`
		} `json:"first_deployed"`
		LastDeployed struct {
			Seconds int `json:"seconds"`
			Nanos   int `json:"nanos"`
		} `json:"last_deployed"`
		Description string `json:"Description"`
	} `json:"info"`
	Namespace string `json:"namespace"`
}

func getDeployment(helmStatus string) string {
	deploymentIndex := -1
	lines := strings.Split(helmStatus, "\n")

	for idx, line := range lines {
		if strings.HasPrefix(line, "==> ") &&
			strings.HasSuffix(line, "/Deployment") {
			deploymentIndex = idx + 2
		}
	}

	sl := strings.Split(lines[deploymentIndex], " ")
	return sl[0]
}

func getPods(helmStatus string) []string {
	podIndexStart := -1
	podIndexEnd   := -1
	lines := strings.Split(helmStatus, "\n")
	podNames := make([]string, 0)
	for idx, line := range lines {
		if strings.HasPrefix(line, "==> ") &&
			strings.Contains(line, "/Pod(") {
			podIndexStart = idx + 2
			continue
		}
		if strings.HasPrefix(line, "==> ") &&
			podIndexStart > 0 &&
			podIndexEnd < 0 {
			podIndexEnd = idx
		}
	}

	for i := podIndexStart; i < podIndexEnd; i++ {
		pl := strings.Split(lines[i], " ")
		podNames = append(podNames, pl[0])
	}

	return podNames
}

func getPodArgs(pods []string) []string {
	podArgs := make([]string, 0)

	podArgs = append(podArgs, "--since")
	podArgs = append(podArgs, "1h")
	for _, pod := range pods {
		podArgs = append(podArgs, "-p")
		podArgs = append(podArgs, pod)
	}

	return podArgs
}

func getTailBin() string {
	pluginPath := os.Getenv("HELM_PLUGIN_DIR")
	logBinary := filepath.Join(pluginPath, "kail")
	return logBinary
}

func tailPods(tailBin string, podNames []string) {
	podArgs := getPodArgs(podNames)
	cmd := exec.Command(tailBin, podArgs...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr	
	err := cmd.Run()
	if err != nil {
		log.Fatalf("tailPods ERRORED with : %v\n", err)

	}
}

func usage() {
	fmt.Printf("Usage: helm logs <helm release>\n")
}

func main() {
	helmRelease := HelmReleaseStatus{}
	if len(os.Args) <= 1 {
		usage()
		os.Exit(1)
	}
	cmd := exec.Command("helm", "status", "-o", "json", os.Args[1])

	output, err := cmd.Output()
	if err != nil {
		log.Fatalf("helm cmd ERRORED with: %v\n", err)
	}
	err = json.Unmarshal(output, &helmRelease)
	if err != nil {
		log.Fatalf("json marshaling ERRORED with: %v\n", err)
	}
	podNames := getPods(helmRelease.Info.Status.Resources)
	tailPods(getTailBin(), podNames)
}
