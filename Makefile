
build:
	go build -o helm-logs helm_logs.go

helm-logs: build

install: helm-logs
	helm plugin install .

clean:
	rm -f helm-logs	

uninstall:
	helm plugin remove logs

run:
	go run helm_logs.go
