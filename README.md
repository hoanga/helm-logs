# Helm Logs Plugin

A Helm plugin to view logs based on a Helm release


## Usage

```shell
helm plugin logs <release>
```

## Install

### Full Helm plugin manager support forthcoming

```shell
git clone <this-url>
cd <cloned-dir>
bash <( curl -sfL https://raw.githubusercontent.com/boz/kail/master/godownloader.sh) -b "$PWD"
make install
```
